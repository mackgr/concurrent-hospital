import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by kamil on 6/10/17.
 */
public class HospitalView extends JPanel implements MouseListener
{
    ArrayList<Ellipse2D> ovals = new ArrayList<>();

    public BottomPanel panel;
    private BufferedImage map;
    private BufferedImage doctorImage;
    private BufferedImage fog;
    private BufferedImage greenLamp;
    private BufferedImage redLamp;
    private BufferedImage revGreenLamp;
    private BufferedImage revRedLamp;
    private BufferedImage patientImage;
    private Hospital hospital;
    private Ellipse2D oval;

    public HospitalView()
    {
        setSize(1200,600);
        try {
            map = ImageIO.read(this.getClass().getClassLoader().getResource("hospital.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            doctorImage = ImageIO.read(this.getClass().getClassLoader().getResource("doctor.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            fog = ImageIO.read(this.getClass().getClassLoader().getResource("mgla.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            greenLamp = ImageIO.read(this.getClass().getClassLoader().getResource("lampaGREEN.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            redLamp = ImageIO.read(this.getClass().getClassLoader().getResource("lampaRED.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            revGreenLamp = ImageIO.read(this.getClass().getClassLoader().getResource("lampaGREENrev.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            revRedLamp = ImageIO.read(this.getClass().getClassLoader().getResource("lampaREDrev.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            patientImage = ImageIO.read(this.getClass().getClassLoader().getResource("pacjent.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        hospital = new Hospital();
        for(int i = 0; i< hospital.getPatients().size(); i++)
        {
            oval = new Ellipse2D.Double(
                    hospital.getPatients().get(i).xPosition,
                    hospital.getPatients().get(i).yPosition,
                    40,
                    40);
            ovals.add(oval);
        }
        this.addMouseListener(this);

    }
    public Hospital getHospital() {
        return hospital;
    }
    @Override
    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.drawImage(map, 0, 0, null);
        g2.setColor(Color.RED);
        ovals = new ArrayList<>();
        for(int i = 0; i< hospital.getPatients().size(); i++) {
            oval = new Ellipse2D.Double(
                    hospital.getPatients().get(i).xPosition,
                    hospital.getPatients().get(i).yPosition,
                    40,
                    40);
            g2.drawImage(patientImage,hospital.getPatients().get(i).xPosition, hospital.getPatients().get(i).yPosition, null);
            //g2.fill(oval);
            ovals.add(oval);
        }
        for(int i = 0; i< hospital.getDoctors().size(); i++)
        {
            g2.drawImage(doctorImage, hospital.getDoctors().get(i).xPosition, hospital.getDoctors().get(i).yPosition, null);
        }
        if(hospital.getDecontaminationRoom().isOccupied()) {
            g2.drawImage(fog, 0, 0, null);
            g2.drawImage(revRedLamp, 140, 270, null);
            g2.drawImage(redLamp, 270, 60, null);
        }
        else{
            g2.drawImage(revGreenLamp, 140, 270, null);
            g2.drawImage(greenLamp, 270, 60, null);
        }


//        for(Chair c : hospital.getWaitingRoom().getChairs())
//        {
//            g2.setColor(Color.blue);
//            g2.fill(new Rectangle2D.Double(c.getxPosition(),c.getyPosition(),20,20));
//        }

    }

    public void mouseClicked(MouseEvent e) {
        for (int i = 0; i < ovals.size(); i++) {
            if( (e.getButton() == 1) && ovals.get(i).contains(e.getX(),e.getY()))
            {
                panel.text.setText(""+hospital.getPatients().get(i).toString());
            }
        }
    }
    @Override
    public void mousePressed(MouseEvent e) {


    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }
    @Override
    public void mouseEntered(MouseEvent e) {


    }

    @Override
    public void mouseExited(MouseEvent e) {


    }


}
