import java.util.Random;

public class Patient extends Person{
    private int health;
    private String disease;
    private Hospital hospital;
    private Random generator = new Random();
    public Patient(String name, String lastName, int health,String disease,Hospital hospital) {
        super(name, lastName);
        this.hospital = hospital;
        this.health = health;
        this.disease = disease;
        xPosition = 1250;
        yPosition = 150;
    }
    @Override
    public void run() {
        super.run();
        int initialX = xPosition;
        int initialY = yPosition;
        goToRegistration();
        hospital.getRegistration().occupyRegistration();
        move(hospital.getWaitingRoom().getxPosition(),hospital.getWaitingRoom().getyPosition(),5);
        Chair chair = hospital.getWaitingRoom().occupyChair();
        move(chair.getxPosition(),chair.getyPosition(),5);
        Bed bed = hospital.getOperatingRoom().reserveBed();
        hospital.getWaitingRoom().releaseChair(chair);
        move(hospital.getDecontaminationRoom().getxPosition(),hospital.getDecontaminationRoom().getyPosition(),5);
        hospital.getDecontaminationRoom().enter(this);
        move(hospital.getOperatingRoom().getxPosition(),hospital.getOperatingRoom().getyPosition(),5);
        hospital.getDecontaminationRoom().leave();
        move(bed.getxPosition(),bed.getyPosition(),10);
        hospital.getOperatingRoom().layOnBed(this,bed);

        hospital.getOperatingRoom().releaseBed(bed);
        move(hospital.getOperatingRoom().getxPosition(),hospital.getOperatingRoom().getyPosition(),10);
        hospital.getDecontaminationRoom().enter(this);
        move(hospital.getDecontaminationRoom().getxPosition(),hospital.getDecontaminationRoom().getyPosition(),10);
        hospital.getDecontaminationRoom().leave();
        move(initialX,initialY,15);
        hospital.getPatients().remove(this);
    }

    public String toString()
    {
        return super.toString()+ " " + health + " " + disease;
    }
    private  void goToRegistration() {
        while(xPosition != hospital.getRegistration().getxPosition())
        {
            xPosition--;
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if(xPosition <= 1050)
            {
                hospital.getRegistration().checkAccesibility();
            }
        }
        while(yPosition != hospital.getRegistration().getyPosition())
        {
            yPosition++;
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            hospital.getRegistration().checkAccesibility();
        }

    }
    private void goToWaitingRoom(){
        while(xPosition != 830)
        {
            xPosition--;
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


        }
        while(yPosition != 50)
        {
            yPosition--;
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }
}
