import java.util.ArrayDeque;
import java.util.Queue;
import java.util.Random;

public class PatientsSpawner implements Runnable{
    private Random generator = new Random();
    private Hospital hospital;
    private Queue<Thread> livingPatients = new ArrayDeque<>();
    private int maxPatients;
    public PatientsSpawner(Hospital hospital,int maxPatients){
        this.hospital = hospital;
        this.maxPatients = maxPatients;
    }

    public void spawnPatients2(){

        while(true) {
            if (livingPatients.size()< maxPatients) {
                hospital.addPatient();
                Thread livingPatient = new Thread(hospital.getPatients().get(hospital.getPatients().size()-1));
                livingPatient.start();
                livingPatients.add(livingPatient);
                try {
                    Thread.sleep(generator.nextInt(70000) + 200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    livingPatients.element().join();
                    livingPatients.remove();
                    hospital.getPatients().remove(hospital.getPatients().size()-1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        }
    }
    public void spawnPatients(){
        for (Patient p : hospital.getPatients()) {
            Thread t = new Thread(p);
            t.start();
            livingPatients.add(t);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    @Override
    public void run() {
        spawnPatients();
        while (true) {
            if (hospital.getPatients().size() < 15) {
                hospital.addPatient();
                Thread p = new Thread(hospital.getPatients().get(hospital.getPatients().size() - 1));
                p.start();
                livingPatients.add(p);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            else{
                try {
                    livingPatients.poll().join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        }
    }
}
