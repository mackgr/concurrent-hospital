import java.util.*;

import static java.lang.Thread.sleep;

/**
 * Created by kamil on 6/6/17.
 */
public class Hospital
{
    private Random generate = new Random();
    private  WaitingRoom waitingRoom = new WaitingRoom();
    private ArrayList<Patient> patients = new ArrayList<>();
    private ArrayList<Doctor> doctors = new ArrayList<>();
    public PatientsSpawner patientsSpawner = new PatientsSpawner(this,10);
    private DecontaminationRoom decontaminationRoom = new DecontaminationRoom();
    private OperatingRoom operatingRoom = new OperatingRoom();
    public Hospital() {
        for (int i = 0 ; i < 10; i++)
            addPatient();
        for (int i = 0 ; i < 2; i++){
            boolean isMale = generate.nextBoolean();
            int skill = generate.nextInt(10) +1;
            if(isMale)
                doctors.add(new Doctor(Names.getMaleName(), Names.getMaleSurname(), skill, this,800 + i*50,500));
            else
                doctors.add(new Doctor(Names.getFemaleName(), Names.getFemaleSurname(), skill, this,800 + i*50,500));
        }
        for(Doctor doctor : doctors){
            new Thread(doctor).start();
        }

    }
    private Registration registration = new Registration();
    public Registration getRegistration() {
        return registration;
    }
    public WaitingRoom getWaitingRoom() {
        return waitingRoom;
    }
    public void addPatient() {
        boolean isMale = generate.nextBoolean();
        int health = generate.nextInt(80) + 10;
        if(isMale)
            patients.add(new Patient(Names.getMaleName(), Names.getMaleSurname(), health, Names.getDisease(),this));
        else
            patients.add(new Patient(Names.getFemaleName(), Names.getFemaleSurname(), health, Names.getDisease(),this));
    }
    public List<Patient> getPatients()
    {
        return this.patients;
    }
    public DecontaminationRoom getDecontaminationRoom() {
        return decontaminationRoom;
    }
    public OperatingRoom getOperatingRoom() {
        return operatingRoom;
    }
    public ArrayList<Doctor> getDoctors() {
        return doctors;
    }
}
