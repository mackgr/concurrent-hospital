public class Doctor extends Person {
    //String specialization;
    int skill;
    int initialX;
    int initialY;
    Hospital hospital;
    public Doctor(String name, String lastName, int skill,Hospital hospital,int x,int y)
    {
        super(name, lastName);
        //this.specialization = specialization;
        this.skill = skill;
        this.hospital = hospital;
        xPosition = x;
        yPosition = y;
        initialX = x;
        initialY = y;
    }

    @Override
    public void run()
    {
        while(true) {
            Bed bed = hospital.getOperatingRoom().waitForPatients(this);
            //bed.setDoctor(this);
            move(xPosition,yPosition - 100,5);
            move(bed.getxPosition(),bed.getyPosition()- 50,5);
            healPatient(bed.getPatient());
            bed.setDoctor(null);
        }

    }
    public int getSkill() {
        return skill;
    }
    public void healPatient(Patient patient){
        while( patient != null && patient.getHealth() < 100){
            patient.setHealth(patient.getHealth()+skill);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        move(xPosition,yPosition - 100,5);
        move(initialX,initialY,5);
    }
}
