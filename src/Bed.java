
public class Bed {
    private  int xPosition;
    private int yPosition;
    private Patient patient;
    private Doctor doctor;
    private boolean isReserved;
    public Bed(int xPosition,int yPosition){
        patient = null;
        patient = null;
        isReserved = false;
        this.xPosition = xPosition;
        this.yPosition = yPosition;
    }
    public int getxPosition() {
        return xPosition;
    }
    public int getyPosition() {
        return yPosition;
    }
    public Patient getPatient(){
        return patient;
    }
    public void setPatient(Patient patient){
        this.patient = patient;
    }
    public void setDoctor(Doctor doctor){
        this.doctor = doctor;
    }
    public Doctor getDoctor() {
        return doctor;
    }
    public boolean isReserved() {
        return isReserved;
    }
    public void setReservation(boolean isReserved) {
         this.isReserved = isReserved;
    }
}
