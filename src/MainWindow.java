import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by kamil on 6/6/17.
 */
public class MainWindow extends JFrame {
    private HospitalView view;
    private BottomPanel bottomPanel;

    public MainWindow() {
        bottomPanel = new BottomPanel();
        //bottomPanel.setLayout(getLayout());
        view = new HospitalView();
        view.panel = bottomPanel;
        add(bottomPanel, BorderLayout.SOUTH);
        add(view, BorderLayout.CENTER);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        setResizable(true);
        //view.setBounds(0,0,1200,800);
        setSize(1400, 800);
        Timer timer = new Timer(1, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                view.repaint();
            }
        });
        timer.start();
        new Thread(view.getHospital().patientsSpawner).start();
    }
}