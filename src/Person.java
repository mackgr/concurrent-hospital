import java.awt.geom.Ellipse2D;

public class Person implements Runnable{
    private String name;
    private String lastName;
    protected int xPosition;
    protected int yPosition;
    private final int dx = 1;
    private final int dy = 1;
    public Person(String name, String lastName) {
        this.name = name;
        this.lastName = lastName;
    }

    public void move() {
        xPosition += dx;

    }

    public Ellipse2D getShape()
    {
        return new Ellipse2D.Double(xPosition, yPosition, 15, 15);
    }

    public String toString() {
        return name +" " +lastName;
    }
    public void move(int destX, int destY,int moveDelay){
        while(xPosition != destX)
        {
            if(xPosition > destX)
                xPosition--;
            else
                xPosition++;
            Thread.yield();
            try {
                Thread.sleep(moveDelay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        while(yPosition != destY)
        {
            if(yPosition > destY)
                yPosition--;
            else
                yPosition++;
            try {
                Thread.sleep(moveDelay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    public void run()
    {
        this.move();
    }
}
